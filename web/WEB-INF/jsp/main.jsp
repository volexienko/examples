<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
  <head>
    <title></title>
  </head>
  <body>
    <h3>Create user</h3>
    <form action="/job" method="POST">
        <input type="hidden" name="action" value="create">
        <input type="text" placeholder="Name" name="name"/>
        <input type="email" placeholder="Email" name="email"/>
        <input type="submit"/>
    </form>

    <br>

    <h3>Get users</h3>
    <form action="/job" method="GET">
        <input type="hidden" name="action" value="list">
        <input type="submit"/>
    </form>

    <hr>

    <h3>Get users</h3>
    <div id="results">
        <c:if test="${newUserId != null}">
            New user was created with id ${newUserId}
        </c:if>
        <c:if test="${users != null}">
            <c:forEach items="${users}" var="u">
                <div>
                    Name: ${u.name}
                    <br>
                    Email: ${u.email}
                </div>
                <br>
            </c:forEach>
        </c:if>
    </div>
  </body>
</html>
