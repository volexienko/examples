package com.lessons.controllers;

import com.lessons.domains.User;
import com.lessons.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class MainController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/")
    public String action() {
        return "main";
    }

    @RequestMapping("/job")
    public ModelAndView job(@RequestParam(required = false) String name, @RequestParam(required = false) String email, @RequestParam(required = true) String action) {
        ModelAndView mav = new ModelAndView("main");
        if ("create".equals(action)) {
            userService.addUSer(name, email);

            //TODO fix me
            mav.addObject("newUserId", "was created");
        } else if ("list".equals((action))) {
            List<User> users = userService.listUsers();
            mav.addObject("users", users);
        }

        return mav;
    }

}
