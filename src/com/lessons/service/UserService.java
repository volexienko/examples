package com.lessons.service;

import com.lessons.dao.CarDAO;
import com.lessons.dao.UserDAO;
import com.lessons.domains.Car;
import com.lessons.domains.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserService {

    //TODO create a separate Service for Cars.
    @Autowired
    UserDAO userDAO;

    @Autowired
    CarDAO carDAO;

    /**
     * Method to CREATE a user in the database
     *
     * @param name  user's name
     * @param email user's email
     */
    public void addUSer(String name, String email) {
        User user = new User();
        user.setEmail(email);
        user.setName(name);
        userDAO.save(user);

        Car car = new Car();
        car.setName("my super car");
        car.setUser(user);
        carDAO.save(car);

        Car car2 = new Car();
        car2.setName("my super car2");
        car2.setUser(user);
        carDAO.save(car2);
    }

    /**
     * Method to READ all the users from DB
     *
     * @return the list of users
     */
    public List<User> listUsers() {
        return userDAO.listUsers();
    }
}
