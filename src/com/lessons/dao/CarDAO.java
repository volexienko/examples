package com.lessons.dao;

import com.lessons.domains.Car;
import org.springframework.stereotype.Repository;

@Repository
public class CarDAO extends BaseDAO {

    public void save(Car car) {
        getSession().save(car);
    }
}
